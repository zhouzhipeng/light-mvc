# lightmvc 超轻量级mvc框架
1.lightmvc 是一个超级轻量级的java web MVC 框架，无xml配置，无注解！

2.符合COC(约定优于配置）原则，只要你约定好，项目代码将非常整洁清爽！

3.非常适合小型java web项目开发，快速、高效、干净！
 
 
##概览
###1.Controller

你的类继承三种Controller之一即可： JSONController、JSONPController、JetxController <br>
顾名思义上述三种Controller可以分别响应: json、jsonp、 html template.

如果你觉得上面三种响应格式无法满足需求，你也可以实现你自己的Controller，仅仅继承com.zhouzhipeng.lightmvc.BaseController 即可。

####JSONController(或JSONPController)例子

    /**
     * 父类Controller中均内置 request、response、session、application变量
     * 此controller默认访问路径为： /yourDemo
     * 响应格式：json
     */
    public YourDemoController extends JSONController{
        
        /**
         * index方法为默认方法，匹配url：/yourDemo/ 或 /yourDemo
         * 响应格式：json
         */
        @Override
        public JSON index(){
            //JSONController父类中提供一些非常实用的工具方法
            
            //比如： 获取请求参数
            String name=param("name");      //获取字符串参数
            int age=param("age",int.class); //获取整型参数
            Date age=param("date",Date.class); //获取日期参数
            ... //诸如此类
            
            //request获取或设置变量
            attr("key","value");    //设置request变量
            String value=attr("key"); //获取request域变量
            
            //session域变量
            session("skey","svalue); //设置session域变量
            String svalue=session("skey"); //获取session域变量
            
            
            //常用的校验方法
            boolean ret=isInt("abc");
            boolean ret2=isEmpty("abc");
            ...
            
            
            //常用的转型方法
            int i=toInt("332");
            boolean b=toBool(List);
            ...
            
            
            //日志方法
            debug("debug log");
            info("info log");
            error("error log");
            ...
            
            
            //非常核心的方法： 获取Service,关于service可参见下节Service篇。
            //1.方式一，按Service的class获取
            DemoService demoService=$(DemoService.class);
            
            //2.方式二，按Model的class获取
            DaoService<Bean> service=$$(Bean.class);
            
           
            //json格式工具方法
            JSON j=toJson(map);
            
            
            return ok_msg("ok"); //返回格式：{ ok :1,msg: "ok"}
        }
        
        
        /**
         * 匹配url：/yourDemo/demo
         * 响应格式：json
         */
        public JSON demo(){
            return ok_msg("demo");
        }
    }
    
约定：继承JSONController或JSONPController的controller中，要被访问url匹配的方法签名必须为：public JSON yourMethodName()，其他方法将不被认为是一个action方法。
     

If you want to configure your own style url name,just use the com.zhouzhipeng.lightmvc.URL annotation,like this:
    
    @URL("/anyUrlName")
    public YourDemoController extends JetxController{
            @Override
            public void index(){
                
            }
            
            @URL("/any")
            public void test(){
                
            }
    }
    
The method "test()" will match /anyUrlName/any . If you dont want to make if fuzzy,just use the default arrange.


###2.Service
As i think, there has only two type of services, one is  Dao-Service which contains some CRUD methods and one is Non-Dao-Service which
contains no CRUD operation methods.

When your service 



##Steps
###1.configure the web.xml
    
    <servlet>
        <servlet-name>LightMVCServlet</servlet-name>

        <servlet-class>com.zhouzhipeng.lightmvc.LightMVCServlet</servlet-class>
        <init-param>
            <param-name>configClass</param-name>
            <param-value>demo.DemoConfig</param-value>
        </init-param>
        <load-on-startup>1</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>LightMVCServlet</servlet-name>
        <url-pattern>/*</url-pattern>
    </servlet-mapping>
    
    
###2.implement the class demo.DemoConfig

    public class  DemoConfig extends LightConfig {
        @Override
        public String db_url() {
            return YOUR_DB_CONN_URL;//eg."jdbc:mysql://****";
        }
    
        @Override
        public String db_user() {
            return YOUR_DB_USER;
        }
    
        @Override
        public String db_pwd() {
            return YOUR_DB_PWD;
        }
    
        @Override
        public String scanPath() {
            // the lightmvc scan package path,put your controllers and services under the scan path 
            // or sub pathes
            return "com.zhouzhipeng.lightmvcdemo";
        }

    }