package com.zhouzhipeng.lightmvc.misc;

import com.zhouzhipeng.lightmvc.monitor.Monitor;
import jetbrick.template.JetEngine;

import java.util.Properties;

/**
 * lightmvc configure class
 * User: zhouzhipeng.com
 * Date: 15/7/30:13:21
 */
public abstract class LightConfig {
    public static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";

    public String db_driver() {
        return MYSQL_DRIVER;
    }

    /**
     * your db connection url. eg: jdbc:mysql://****
     * the default jdbc driver is mysql. You can override
     * the method db_driver() to adjust
     *
     * @return
     */
    public abstract String db_url();

    /**
     * your db username
     *
     * @return
     */
    public abstract String db_user();


    /**
     * your db password
     *
     * @return
     */
    public abstract String db_pwd();

    /**
     * the lightmvc scan package path,put your controllers and services under the scan path
     * or sub pathes
     *
     * @return
     */
    public abstract String scanPath();


    /**
     * your view pages root path.  eg: /WEB-INF
     *
     * @return
     */
    public String viewRoot() {
        return "/WEB-INF";
    }


    /**
     * extra jetbrick engine configure.
     * see : http://subchen.github.io/jetbrick-template/2x/config.html
     *
     * @return
     */
    public Properties jetConfig() {
        return null;
    }


    /**
     * 启动时回调
     */
    public void onStartUp(){};

    /**
     * 应用监视器,可以监控基于每次请求路径轨迹、CPU使用、内存使用量
     * 默认不实现
     * @return
     */
    public Monitor monitor() {
        return null;
    }

}
