
package com.zhouzhipeng.lightmvc.utils;


import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBUtils {



    private static Logger logger = Logger.getLogger(DBUtils.class);
    public  static boolean isInited = false;//是否已初始化


    private static ConnectionPool pool; //连接池

    public static void init(String driver,String url,String user,String pwd) {

        try {
            if (isInited) return;

            isInited = true;


            pool = new ConnectionPool(driver, url, user, pwd);
            pool.setTestTable("subject");
            pool.createPool();

            logger.info("加载mysql驱动，以及创建连接池..");
        } catch (Exception e) {
            logger.error("连接池初始化异常", e);
        }
    }


    public static void free(Connection conn) throws Exception {
        logger.info("free connection!");
        pool.returnConnection(conn);
    }

    public static Connection getConnection() throws Exception {

        Connection connection = threadLocal.get();
        if(connection==null){
            connection=pool.getConnection();
            logger.info("get connection from pool!");
        }else{
            logger.info("get connection from threadLocal!");
        }


        return connection;
    }

    public static Connection getConnFromThreadLocal(){
        return threadLocal.get();
    }

    public static void setThreadLocalConnection(Connection conn){
        logger.info("setThreadLocalConnection");
        threadLocal.set(conn);
    }

    public static void removeThreadLocalConnection(){
        logger.info("removeThreadLocalConnection");
        threadLocal.remove();
    }


    //线程变量，用于控制事务
    private static final ThreadLocal<Connection> threadLocal=new ThreadLocal<Connection>();

    /**
     * select,insert ,update ,delete语句的公共方法;   千万注意values 中数据的顺序要与？的顺序一致
     *
     * @param sql    sql语句
     * @param values 一定要按？出现的顺序（如果有的话，否则传null）将参数值填充到values数组中
     * @throws Exception
     */
    public static <T> T execute(String sql, Object... values) throws Exception {

        if (sql == null)
            throw new IllegalArgumentException("sql参数不能为空");

        Connection conn = null;
        PreparedStatement ps=null;
        ResultSet rs = null;


        try {
            conn = getConnection();
            ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            if (values != null) {
                for (int i = 0; i < values.length; i++) {
                    ps.setObject(i + 1, values[i]);
                }
            }

            if (sql.startsWith("select")) {
                rs = ps.executeQuery();

                List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
                ResultSetMetaData metaData = rs.getMetaData();


                while (rs.next()) {

                    Map<String,Object> map=new HashMap<String, Object>();
                    for (int i = 1; i <= metaData.getColumnCount(); i++) {
                        String columnName = metaData.getColumnName(i);
                        Object value = rs.getObject(columnName);
                        map.put(columnName, value);
                    }

                    list.add(map);
                }

                return (T) list;
            } else {
                ps.executeUpdate();
                rs = ps.getGeneratedKeys();

                if(rs.next()){
                    return (T)rs.getObject(1);
                }
            }




        } finally {
            if(rs!=null){
                rs.close();
            }
            if(ps!=null){
                ps.close();
            }

            if(threadLocal.get()==null) {
                free(conn);
            }
        }

        return null;


    }


    public static void destroy() {

        try {
            pool.closeConnectionPool();
            logger.info("释放数据库连接并取消注册数据库驱动。");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }


    public static ArrayList<ArrayList<Object>> getTableInfo(String tableName) throws Exception {
        Connection conn = getConnection();
        DatabaseMetaData dbmd = conn.getMetaData();

        String catalog = conn.getCatalog();
        ResultSet rs = dbmd.getTables(catalog, "%", tableName, new String[]{"TABLE"});


        ArrayList<ArrayList<Object>> list=new ArrayList<ArrayList<Object>>();

        if (rs.next()) {
            String s = rs.getString("TABLE_NAME");
            String type = rs.getString("TABLE_TYPE");
            if (type.equalsIgnoreCase("table") && !s.contains("$")) {


                String columnName;
                String columnType;
                ResultSet colRet = dbmd.getColumns(catalog, "%", s, "%");
                while (colRet.next()) {
                    columnName = colRet.getString("COLUMN_NAME");
                    columnType = colRet.getString("TYPE_NAME");
                    int columnSize = colRet.getInt("COLUMN_SIZE");
                    int nullable = colRet.getInt("NULLABLE");
                    String defaultVal = colRet.getString("COLUMN_DEF");


                    ArrayList<Object> subList=new ArrayList<Object>();
                    subList.add(columnName);
                    subList.add(columnType);
                    subList.add(columnSize);
                    subList.add(nullable==1);
                    subList.add(defaultVal);

                    list.add(subList);

                }


                colRet.close();
            }
        }

        rs.close();

        free(conn);


        return list;
    }


}