package com.zhouzhipeng.lightmvc.utils;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

/**
 * 工具类
 * User: zhouzhipeng.com
 * Date: 15/8/4:17:29
 */
public class LightUtils {

    /**
     * 获取当前空闲内存大小
     * @return
     */
    public static long getMemory() {
        return Runtime.getRuntime().freeMemory();
    }

    public static long getCPUTime(){
        ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();

        return threadMXBean.getCurrentThreadCpuTime();

    }
}
