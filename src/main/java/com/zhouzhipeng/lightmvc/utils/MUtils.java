package com.zhouzhipeng.lightmvc.utils;

import com.zhouzhipeng.lightmvc.misc.LightConfig;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;

/**
 * TODO:写描述
 * User: zhouzhipeng
 * Date: 15/7/24:11:16
 */
public class MUtils {


    public static void generateModelClass(String driver,String url,String user,String pwd,String tablename,String packageName) throws Exception{

        DBUtils.init(driver,url,user,pwd);

        ArrayList<ArrayList<Object>> tableInfo = DBUtils.getTableInfo(tablename);

    }

    public static String exportDML(Class<?> clazz){
        String tableName = clazz.getSimpleName();
        String sql="create table "+tableName+"(\n";

        for (Field field : clazz.getDeclaredFields()) {

            field.setAccessible(true);
            String name = field.getName();
            String type = field.getType().getSimpleName();

            type=parseType(type);


            sql+=name+" "+type+" null,\n";
        }

        sql=sql.substring(0,sql.length()-2);

        sql+=");\n";

        return sql;
    }

    private static String parseType(String type) {
        //TODO:按照mysql类型来解析
        if(type.equals("String")){
            return "varchar(100)";
        }else if(type.equals("Integer")){
            return "integer";
        }

        return null;
    }


    public static String join(Collection obj, String separator) {
        String c = "";
        for (Object o : obj) {
            if (o instanceof String) {
                c += "'" + o + "'" + separator;
            } else {
                c += o + separator;
            }
        }

        c = c.substring(0, c.length() - 1);

        return c;
    }
}
