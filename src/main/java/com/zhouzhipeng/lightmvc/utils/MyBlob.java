package com.zhouzhipeng.lightmvc.utils;


import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;

public class MyBlob implements Blob {

    private byte[] _bytes = new byte[0];
    private long offsetPos=0;
    private long length=0;

    private MyBlob(){}
    public static final MyBlob emptyBlob=new MyBlob();

    public MyBlob(byte[] buf) {
        this(buf, 0, buf.length);
    }

    public MyBlob(byte[] buf, int offset, int len) {
        if (offset < 0 || offset >= buf.length) {
            throw new IllegalArgumentException("offset");
        }

        if (len < 0) {
            throw new IllegalArgumentException("len");
        }
        _bytes = buf;
        offsetPos = offset;
        length = Math.min(len, buf.length - offset);
    }

    @Override
    public long length() throws SQLException {
        return length;
    }

    @Override
    public byte[] getBytes(long pos, int length) throws SQLException {
        if (pos < 1 || length < 0) {
            throw new SQLException("pos,length");
        }

        long start = pos - 1 + offsetPos;
        long end = start + Math.min(this.length, length);

        byte[] newBytes = new byte[(int) (end - start)];
        for (int i = 0; i < newBytes.length; i++) {
            newBytes[i] = _bytes[(int) (start + i)];
        }

        return newBytes;
    }

    @Override
    public InputStream getBinaryStream() throws SQLException {
        try {
            return new ByteArrayInputStream(_bytes, (int) offsetPos, (int) length);
        } catch (Exception e) {
            throw new SQLException(e);
        }

    }

    @Override
    public long position(byte[] pattern, long start) throws SQLException {
//        if(start<1){
//            throw new SQLException("start must ge 1");
//        }


        throw new SQLFeatureNotSupportedException();

    }

    @Override
    public long position(Blob pattern, long start) throws SQLException {
        throw new SQLFeatureNotSupportedException();
    }

    @Override
    public int setBytes(long pos, byte[] bytes) throws SQLException {
        throw new SQLFeatureNotSupportedException();
    }

    @Override
    public int setBytes(long pos, byte[] bytes, int offset, int len) throws SQLException {
        throw new SQLFeatureNotSupportedException();
    }

    @Override
    public OutputStream setBinaryStream(long pos) throws SQLException {
        throw new SQLFeatureNotSupportedException();
    }

    @Override
    public void truncate(long len) throws SQLException {
        throw new SQLFeatureNotSupportedException();
    }

    @Override
    public void free() throws SQLException {
        _bytes = new byte[0];
        offsetPos = 0;
        length = 0;
    }

    @Override
    public InputStream getBinaryStream(long pos, long length) throws SQLException {
        return new ByteArrayInputStream(getBytes(pos, (int) length));
    }
}
