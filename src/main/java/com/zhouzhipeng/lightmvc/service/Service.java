package com.zhouzhipeng.lightmvc.service;

import com.zhouzhipeng.lightmvc.LightMVCServlet;
import com.zhouzhipeng.lightmvc.TX;
import com.zhouzhipeng.lightmvc.monitor.Contrail;
import com.zhouzhipeng.lightmvc.utils.DBUtils;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.apache.log4j.Logger;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.sql.Connection;
import java.util.*;

/**
 * Service父类
 * User: zhouzhipeng.com
 * Date: 15/8/3:12:13
 */
public abstract class Service {

    private static Logger __log = Logger.getLogger(Service.class);

    protected Logger logger = Logger.getLogger(this.getClass());

    protected void debug(String str) {
        logger.debug(str);
    }

    protected void info(Object str) {
        logger.info(str);
    }

    protected void warn(Object str) {
        logger.warn(str);
    }

    protected void error(Object str) {
        logger.error(str);
    }

    protected void error(Object message, Throwable t) {
        logger.error(message, t);
    }


//    private static Map<String, Class> map=null;

    private static ThreadLocal<Contrail> contrailThreadLocal;

    private static Map<Class<? extends Service>, Service> instanceMap = null;

    private static void init(List<Class<? extends Service>> services,ThreadLocal<Contrail> con){
        contrailThreadLocal=con;

        instanceMap = new HashMap<Class<? extends Service>, Service>();

        for(Class<? extends Service> serviceClass:services){
            Enhancer enhancer = new Enhancer();
            enhancer.setSuperclass(serviceClass);
            enhancer.setCallback(new MethodInterceptor() {
                @Override
                public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                    if(!Modifier.isPublic(method.getModifiers())||(Modifier.isNative(method.getModifiers()))
                            ||method.equals(Object.class.getMethod("hashCode"))||method.equals(Object.class.getMethod("toString"))
                            ||method.getName().equals("modelToTable")||method.getName().equals("primaryKey")){
                        return methodProxy.invokeSuper(o, objects);
                    }


                    Contrail.Entity entity = null;

                    if (LightMVCServlet.isMonitorOn()) {
                        __log.info("++++++++ begin service contrail ++++++++++");

                        Contrail contrail = contrailThreadLocal.get();

                        if(contrail!=null) {

                            entity = new Contrail.Entity();

                            entity.setInputs(objects);
                            entity.setStartTime(new Date().getTime());
                            entity.setMethod(method.toString());
                            contrail.getList().add(entity);

                            __log.info("contrail>>" + contrail);
                        }
                    }


                    Object ret=null;

                    if (method.getAnnotation(TX.class) != null) {
                        __log.info("method:" + method.getName());

//TX                      System.out.println("proxy prefix...");
                        Connection connection = DBUtils.getConnFromThreadLocal();

                        if (connection == null) {
                            __log.info("++++ begin transaction ++++");

                            connection = DBUtils.getConnection();
                            DBUtils.setThreadLocalConnection(connection);


                            //关闭默认提交
                            connection.setAutoCommit(false);


                            try {
                                ret = methodProxy.invokeSuper(o, objects);

                                if (!connection.getAutoCommit()) {
                                    connection.commit();
                                    __log.info("commit transaction");
                                }

                            } catch (Exception e) {
                                __log.error(e.getMessage(), e);
                                //rollback
                                connection.rollback();

                                __log.info("---- rollback transaction ----");

                                __log.info("throw outside exception");
                                throw new RuntimeException(e);
                            } finally {
                                connection.setAutoCommit(true);
                                DBUtils.free(connection);
                                //remove threadlocal
                                DBUtils.removeThreadLocalConnection();

                                __log.info("---- release transaction ----");

                                __log.info("---- end transaction ----");
                            }
                        } else {
                            ret = methodProxy.invokeSuper(o, objects);
                        }


                    } else {
                        ret=methodProxy.invokeSuper(o, objects);

                    }

                    if(entity!=null){
                        entity.setEndTime(new Date().getTime());
                        entity.setOutput(ret);

                        __log.info("------- end service contrail -------");
                        __log.info("contrail>>" + contrailThreadLocal.get());
                    }

                    return ret;
                }
            });

            Service  instance = (Service) enhancer.create();

            instanceMap.put(serviceClass, instance);
        }

    }


    /**
     * 根据Model class获取service
     *
     * @param domainClass
     * @param <T>
     * @return
     */
    public static <T> DaoService<T> $$(Class<T> domainClass) {

        DaoService<T> tDaoService = new DaoService<T>(domainClass);
        tDaoService.contrail=contrailThreadLocal.get();

        return tDaoService;
    }

    /**
     * 获取特定的service
     *
     * @param serviceClass
     * @param <T>
     * @return
     */
    public static <T extends Service> T $(Class<T> serviceClass) {

        Service instance = instanceMap.get(serviceClass);

        if(instance instanceof DaoService){
            ((DaoService)instance).contrail=contrailThreadLocal.get();
        }

        if (instance== null) {
            __log.error("service " + serviceClass.getName() + " is not configured!");
        }


        return (T) instance;


    }
}
