package com.zhouzhipeng.lightmvc.service;

import com.zhouzhipeng.lightmvc.LightMVCServlet;
import com.zhouzhipeng.lightmvc.model.Cond;
import com.zhouzhipeng.lightmvc.model.Criteria;
import com.zhouzhipeng.lightmvc.model.M;
import com.zhouzhipeng.lightmvc.monitor.Contrail;
import org.apache.log4j.Logger;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 基础service类
 * User: zhouzhipeng
 * Date: 15/7/27:11:05
 */
public class DaoService<T> extends Service{

    private static Logger log = Logger.getLogger(DaoService.class);




    private HashMap<String,String> _modelToTable;

    /**
     * orm映射,javabean name -》 table field name
     * @return
     */
    public HashMap<String,String> modelToTable(){
        return null;
    }

    /**
     * 主键字段名称(javabean)
     * @return
     */
    public String primaryKey(){
        return M.PRIMARY_KEY_NAME;
    }

    public final Class<T> getModelClass(){
        return this.clazz;
    }

    private Class<T> clazz = null;

//    public void setModelClass(Class<T> modelClass){
//        clazz=modelClass;
//    }

     DaoService(Class<T> modelClass){
        this.clazz=modelClass;
    }

    Contrail contrail;

    protected DaoService() {
        try {

            _modelToTable=modelToTable();

            Type genericSuperclass = this.getClass().getGenericSuperclass();
            Class superClass=this.getClass().getSuperclass();
            while(superClass!=Service.class&&(!(genericSuperclass instanceof ParameterizedType))){
                genericSuperclass=superClass.getGenericSuperclass();
                superClass=superClass.getSuperclass();
            }

            if(genericSuperclass instanceof ParameterizedType){
                clazz = (Class<T>) ((ParameterizedType)genericSuperclass).getActualTypeArguments()[0];
            }else{
                throw new RuntimeException("the class "+this.getClass().getName()+" must assign a generic Type when extends "+DaoService.class.getName());
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public  T get(long id) {
        try {
            contrailBefore(DaoService.class.getMethod("get",long.class),id);

            T ret = M.r(clazz, primaryKey(), _modelToTable, id);

            contrailAfter(ret);
            return ret;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }

    public  List<T> list(Criteria cri) {
        try {
            contrailBefore(DaoService.class.getMethod("list",Criteria.class),cri);

            cri.setClazz(clazz);
            cri.setModelToTable(_modelToTable);
            List<T> ret = M.r(cri, _modelToTable);

            contrailAfter(ret);

            return ret;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return new ArrayList<T>();
    }

    private void contrailBefore(Method method,Object... args){
        if (this.getClass()==DaoService.class&& LightMVCServlet.isMonitorOn()&&contrail!=null) {
            log.info("++++++++ begin dao-service contrail ++++++++++");


            if(contrail!=null) {

                Contrail.Entity entity = new Contrail.Entity();

                entity.setInputs(args);
                entity.setStartTime(new Date().getTime());
                entity.setMethod(method.toString());
                contrail.getList().add(entity);

                log.info("contrail>>" + contrail);
            }
        }

    }

    private void contrailAfter(Object ret){
        if(this.getClass()==DaoService.class&& LightMVCServlet.isMonitorOn()&&contrail!=null){
            List<Contrail.Entity> list = contrail.getList();
            Contrail.Entity entity=list.get(list.size()-1);

            entity.setEndTime(new Date().getTime());
            entity.setOutput(ret);

            log.info("------- end dao-service contrail -------");
            log.info("contrail>>"+contrail);
        }

    }




    public  long create(T t) throws Exception {
        contrailBefore(DaoService.class.getMethod("create",Object.class),t);
        long ret = M.c(t, primaryKey(), _modelToTable);
        contrailAfter(ret);
        return ret;
    }


    public  void update(T t) throws Exception {
        contrailBefore(DaoService.class.getMethod("update",Object.class),t);
        M.u(t,primaryKey(),_modelToTable);
        contrailAfter(null);
    }

    public void update(Cond cond) throws Exception {
        contrailBefore(DaoService.class.getMethod("update",Cond.class),cond);
        cond.setModelToTable(_modelToTable);
        cond.setTableClass(clazz);
        M.u(cond,_modelToTable);
        contrailAfter(null);
    }


    public  void delete(long id) throws Exception {
        contrailBefore(DaoService.class.getMethod("delete",long.class),id);
        M.d(clazz,primaryKey(),_modelToTable, id);

        contrailAfter(null);
    }


}
