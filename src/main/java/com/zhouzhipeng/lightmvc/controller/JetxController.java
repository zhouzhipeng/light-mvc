package com.zhouzhipeng.lightmvc.controller;

import com.zhouzhipeng.lightmvc.LightMVCServlet;
import jetbrick.template.JetEngine;
import jetbrick.template.JetTemplate;
import jetbrick.template.web.JetWebEngine;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Properties;

/**
 *
 * http://subchen.github.io/jetbrick-template/2x/quickstart.html
 *
 * jetbrick-template模板引擎
 * <p/>
 * User: zhouzhipeng
 * Date: 15/7/18:23:20
 */
public abstract class JetxController extends BaseController{

    private final HashMap<String,Object> model=new HashMap<String, Object>();

    public abstract String index() throws Exception;

    protected final String DEFAULT_VIEW=null;


    /**
     * 视图文件扩展名，默认为html
     * @return
     */
    protected String exFilename(){
        return "html";
    }

    private static JetEngine engine =null;

    @Override
    protected String exceptionHandle(Exception e) {
        return e.getClass().getSimpleName()+":"+e.getMessage();
    }

    protected  void model(String name,Object val){
        model.put(name,val);
    }

    protected  void model(HashMap<String,Object> map){
        model.putAll(map);
    }


    @Override
    protected final void init() {
        response.setCharacterEncoding("utf-8");
        response.setContentType("text/html");
        model.clear();
    }


    @Override
    protected  Class[] getMethodReturnType() {
        return new Class[]{String.class,void.class};
    }

    @Override
    protected <T> String getRespStr(T path) {

        String thePath= (String) path;
        if(thePath==null){
            //使用约定的路径
            thePath=attr(FULL_REQ_URI)+"."+exFilename();
        }
//
        info("view path:" + thePath);



        //config

        // 1. 创建一个默认的 JetEngine
        if(engine==null) {
            Properties p=new Properties();

            Properties properties = LightMVCServlet.lightConfig().jetConfig();
            if(properties!=null){
                p.putAll(properties);
            }

            p.setProperty("jetx.template.loaders","$webLoader");

            p.setProperty("$webLoader","jetbrick.template.loader.ServletResourceLoader");
            p.setProperty("$webLoader.root", LightMVCServlet.lightConfig().viewRoot());
            p.setProperty("$webLoader.reloadable","true");


            engine = JetWebEngine.create(application, p, null);

        }



        // 2. 获取一个模板对象 (从默认的 classpath 下面)
        JetTemplate template = engine.getTemplate(thePath);



        // 3. 渲染模板到自定义的 Writer
        StringWriter writer = new StringWriter();
        template.render(model, writer);


        return writer.toString();
    }
}
