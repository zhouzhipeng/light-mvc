package com.zhouzhipeng.lightmvc.controller;

/**
 * TODO:写描述
 * User: zhouzhipeng
 * Date: 15/7/29:11:04
 */
public abstract class JSONPController extends JSONController {
    @Override
    protected void init() {
        response.setContentType("text/html; charset=UTF-8");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");

    }

    @Override
    protected <T> String getRespStr(T str) {

        //检查jsonp参数是否有
        if(!notEmpty(param("jsonp"))) throw new IllegalArgumentException("jsonp is required!");

        return param("jsonp")+"("+str.toString()+")";
    }

    @Override
    protected String exceptionHandle(Exception e) {
        return param("jsonp")+"("+error(e.getClass().getSimpleName()+":"+e.getMessage()).toString()+")";
    }
}
