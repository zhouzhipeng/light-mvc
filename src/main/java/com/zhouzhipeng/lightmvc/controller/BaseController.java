package com.zhouzhipeng.lightmvc.controller;

import com.zhouzhipeng.lightmvc.LightMVCServlet;
import com.zhouzhipeng.lightmvc.URL;
import com.zhouzhipeng.lightmvc.monitor.Contrail;
import com.zhouzhipeng.lightmvc.service.DaoService;
import com.zhouzhipeng.lightmvc.service.Service;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.text.ParseException;
import java.util.*;


/**
 * Controller通用父类
 * <p/>
 * User: zhouzhipeng
 * Date: 15/7/18:23:20
 */
public abstract class BaseController implements Controller {

    protected Logger logger = Logger.getLogger(this.getClass());

    protected void debug(String str) {
        logger.debug(str);
    }

    protected void info(Object str) {
        logger.info(str);
    }

    protected void warn(Object str) {
        logger.warn(str);
    }

    protected void error(Object str) {
        logger.error(str);
    }

    protected void error(Object message, Throwable t) {
        logger.error(message, t);
    }


    private Logger log = Logger.getLogger(BaseController.class);
    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected HttpSession session;
    protected ServletContext application;

    private Map<String, Method> mappings;

    protected abstract <T> String getRespStr(T str);

    protected abstract Class[] getMethodReturnType();

    /*
        校验方法
     */
    protected boolean isEmpty(Object obj) {
        return !notEmpty(obj);
    }

    protected boolean notEmpty(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof String) {
            return !obj.equals("");
        } else if (obj instanceof Collection) {
            return !((Collection) obj).isEmpty();
        } else if (obj instanceof Map) {
            return !((Map) obj).isEmpty();
        }

        return true;
    }


    protected boolean isInt(String str) {
        return str.matches("^[1-9][0-9]*$");
    }

    protected boolean isLong(String str) {
        return isInt(str);
    }

    protected boolean isFloat(String str) {
        return str.matches("^[0-9]+\\.[0-9]+$");
    }

    protected Integer toInt(String str) {
        return Integer.parseInt(str);
    }

    protected Boolean toBool(Object obj) {

        if (obj instanceof String) {
            return Boolean.parseBoolean((String) obj);
        } else if (obj instanceof Number) {
            return !((Number) obj).equals(0);
        }

        return notEmpty(obj);
    }

    protected void init() {
    }

    protected Long toLong(String str) {
        return Long.parseLong(str);
    }

    protected Float toFloat(String str) {
        return Float.parseFloat(str);
    }

    protected Date toDate(String str, String... pattern) throws ParseException {
        return DateUtils.parseDate(str, pattern);
    }


    public void handle(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter writer = null;
        try {

            this.request = request;
            this.response = response;
            this.session = request.getSession();
            this.application = this.session.getServletContext();


            //for subclass init
            init();

            request.setCharacterEncoding("utf-8");
            writer = response.getWriter();


            if (mappings == null) {
                mappings = new HashMap<String, Method>();

                //URL annotation first
                // the method signature must match:  public JSON methodName()

                for (Method method : this.getClass().getDeclaredMethods()) {

                    if (Modifier.isPublic(method.getModifiers()) && containsClass(method.getReturnType(), getMethodReturnType()) && method.getParameterTypes().length == 0) {

                        if (method.getAnnotation(URL.class) != null) {
                            mappings.put(method.getAnnotation(URL.class).value(), method);
                        } else {
                            //match method name
                            if(method.getName().equals(DEFAULT_METHOD_NAME)){
                                mappings.put("/", method);
                                attr(FULL_REQ_URI,attr(FULL_REQ_URI)+"index");
                            }else {
                                mappings.put("/" + method.getName(), method);
                            }
                        }
                    }
                }


            }

            //dispatch request
            String suffixUri = attr(SUFFIX_REQ_URI);

            Method method = mappings.get(suffixUri);
            if (method != null) {
                Contrail.Entity entity = null;
                Contrail contrail=null;
                if (LightMVCServlet.isMonitorOn()) {
                    log.info("++++++++ begin controller contrail ++++++++++");

                    contrail = LightMVCServlet.getThreadLocalContrail();

                    if(contrail!=null) {

                        entity = new Contrail.Entity();

                        entity.setStartTime(new Date().getTime());
                        entity.setMethod(method.toGenericString());
                        contrail.getList().add(entity);

                        log.info("contrail>>" + contrail);
                    }
                }

                Object ret = method.invoke(this);
                writer.write(getRespStr(ret));


                if(entity!=null){
                    entity.setEndTime(new Date().getTime());
                    entity.setOutput(ret);

                    log.info("------- end controller contrail -------");
                    log.info("contrail>>"+contrail);
                }

            } else {
                //not found
                response.setStatus(404);

                String msg="requested uri " + attr(FULL_REQ_URI) + " not found!!";

                log.error(msg);

                writer.write(exceptionHandle(new RuntimeException(msg)));
            }

        } catch (Exception e) {
            response.setStatus(500);
            if (writer != null) {
                writer.write(exceptionHandle(e));
            }
            log.error(e.getMessage(), e);

            if (LightMVCServlet.isMonitorOn()) {


                Contrail contrail = LightMVCServlet.getThreadLocalContrail();

                contrail.setException(e);
                log.info("------- end controller contrail -------");
            }
        } finally {
            if (writer != null) {
                writer.flush();
                writer.close();
            }
        }

    }

    private boolean containsClass(Class<?> returnType, Class[] methodReturnType) {

        for (Class clazz : methodReturnType) {
            if (returnType == clazz) {
                return true;
            }
        }

        return false;
    }


    protected abstract String exceptionHandle(Exception e);

    /**
     * 按Model获取service
     *
     * @param domainClass
     * @return
     * @throws Exception
     */
    protected <T> DaoService<T> $$(Class<T> domainClass){
        return DaoService.$$(domainClass);
    }

    /**
     * 获取service
     *
     * @param serviceClass
     * @return
     * @throws Exception
     */
    protected <T extends Service> T $(Class<T> serviceClass){
        return Service.$(serviceClass);
    }

    /**
     * 获取请求头
     * @param key
     * @return
     */
    protected String header(String key){
        return request.getHeader(key);
    }

    protected String param(String paramName) {
        return request.getParameter(paramName);
    }

    protected String param(String paramName,String defaultVal) {
        String val=param(paramName);
        if(val==null){
            val=defaultVal;
        }

        return val;
    }

    protected void session(String name, Object obj) {

        session.setAttribute(name, obj);
    }

    protected void attr(String name, Object obj) {
        request.setAttribute(name, obj);
    }


    protected <T> T attr(String name) {
        return (T) request.getAttribute(name);
    }


    protected <T> T session(String name) {
        return (T) session.getAttribute(name);
    }

    protected <T> T param(String name, Class<T> clazz,T defaultVal) throws Exception {
        T val=param(name,clazz);
        if(val==null){
            val=defaultVal;
        }

        return val;
    }

    protected <T> T param(String name, Class<T> clazz) throws Exception {
        String val = param(name);
        if (isEmpty(val) || isEmpty(clazz)) return null;

        if (clazz == int.class) {
            return (T) toInt(val);
        } else if (clazz == long.class) {
            return (T) toLong(val);
        } else if (clazz == float.class) {
            return (T) toFloat(val);
        } else if (clazz == boolean.class) {
            return (T) toBool(val);
        } else if (clazz == Date.class) {
            return (T) toDate(val, "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "HH:mm:ss", "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss");
        }else if(clazz== JSONObject.class){
            return (T)JSONObject.fromObject(val);
        }else if(clazz== JSONArray.class){
            return (T)JSONArray.fromObject(val);
        }

        return (T) val;

    }

    protected String[] params(String name) {
        return request.getParameterValues(name);
    }

}
