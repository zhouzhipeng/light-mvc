package com.zhouzhipeng.lightmvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * TODO:写描述
 * User: zhouzhipeng
 * Date: 15/7/28:11:26
 */
public interface Controller {
    String SUFFIX_REQ_URI ="__suffix_uri__";
    String FULL_REQ_URI="__full_req_uri__";
    String DEFAULT_METHOD_NAME="index";



    void handle(HttpServletRequest request, HttpServletResponse response);
}
