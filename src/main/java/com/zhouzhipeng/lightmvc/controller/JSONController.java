package com.zhouzhipeng.lightmvc.controller;

import net.sf.json.JSON;
import net.sf.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * JSON 响应格式父类
 * <p/>
 * User: zhouzhipeng
 * Date: 15/7/18:23:20
 */
public abstract class JSONController extends BaseController{


    @Override
    protected  Class[] getMethodReturnType() {
        return new Class[]{JSON.class};
    }

    @Override
    protected void init() {
        response.setCharacterEncoding("utf-8");
        response.setContentType("text/json");
    }

    @Override
    protected <T> String getRespStr(T str) {
        return str.toString();
    }

    protected JSON toJson(Object obj){
        return JSONObject.fromObject(obj);
    }



    @Override
    protected String exceptionHandle(Exception e) {
        return error(e.getClass().getSimpleName()+":"+e.getMessage()).toString();
    }


    private final String MAP_ATTRIBUTE_NAME = "_retmap__";
    private final String ARRAY_ATTRIBUTE_NAME = "_retarray__";
    private final String TEMP_TO_MAP_ATTRIBUTE_NAME = "_tmptomap__";
    private final String CATTX_ATTRIBUTE_NAME = "_cattx__";
    private final String NEWVALUE_ATTRIBUTE_NAME = "_newretvalue__";

    protected JSONController map(String key, Object value) {
        Map<String, Object> map = (Map<String, Object>) request.getAttribute(MAP_ATTRIBUTE_NAME);
        if (map == null) {
            map = new HashMap<String, Object>();
            request.setAttribute(MAP_ATTRIBUTE_NAME, map);
        }

        map.put(key, value);
        return this;
    }

    protected JSONController array(String key, Object... value) {
        Map<String, List> map = (Map<String, List>) request.getAttribute(ARRAY_ATTRIBUTE_NAME);
        if (map == null) {
            map = new HashMap<String, List>();
            request.setAttribute(ARRAY_ATTRIBUTE_NAME, map);
        }

        List list = map.get(key);
        if(list==null){
            list=Arrays.asList(value);
            map.put(key,list);
        }else {

            list.addAll(Arrays.asList(value));
        }

        return this;
    }

    protected JSONController combine(Map<String, Object> smap) {
        Map<String, Object> map = (Map<String, Object>) request.getAttribute(MAP_ATTRIBUTE_NAME);
        if (map == null) {
            map = new HashMap<String, Object>();
            request.setAttribute(MAP_ATTRIBUTE_NAME, map);
        }

        map.putAll(smap);
        return this;
    }


    protected Map<String, Object> projectMap(Object obj, Map<String, String> mappings) {
        if (!(obj instanceof List)) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            Class clazz = obj.getClass();

            HashMap<String,FnCallbck> fnMap=(HashMap<String,FnCallbck>)  request.getAttribute(NEWVALUE_ATTRIBUTE_NAME);

            //按照普通的bean处理
            for (String key : mappings.keySet()) {
                //转成标准的get方法
                try {
                    Object result = clazz.getMethod("get" + (key.charAt(0) + "").toUpperCase() + key.substring(1)).invoke(obj);

                    String newkey = mappings.get(key);

                    if(fnMap!=null&&fnMap.get(newkey)!=null){
                        result=fnMap.get(newkey).getNewVal(result);
                    }

                    map.put(newkey, result);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }

            return map;
        }

        return null;
    }

    protected List<Map<String, Object>> projectList(List li, Map<String, String> mappings) {

        List<Map<String, Object>> retList = new ArrayList<Map<String, Object>>();
        for (Object ob : li) {
            retList.add(projectMap(ob, mappings));
        }

        return retList;
    }

    protected JSONController p(String from, String to) {
        Map<String, String> map = (Map<String, String>) request.getAttribute(TEMP_TO_MAP_ATTRIBUTE_NAME);
        if (map == null) {
            map = new HashMap<String, String>();
            request.setAttribute(TEMP_TO_MAP_ATTRIBUTE_NAME, map);
        }

        map.put(from, to);

        return this;
    }

    public static interface FnCallbck{
        public Object getNewVal(Object old);
    }

    protected JSONController p(String from, String to,FnCallbck callback) {
        Map<String, FnCallbck> map = (Map<String, FnCallbck>) request.getAttribute(NEWVALUE_ATTRIBUTE_NAME);
        if (map == null) {
            map = new HashMap<String, FnCallbck>();
            request.setAttribute(NEWVALUE_ATTRIBUTE_NAME, map);
        }

        p(from,to);

        map.put(to, callback);

        return this;
    }

    protected JSONController p(String from) {
        return p(from, from);
    }


    protected Map<String, String> p() {
        Map<String, String> map = (Map<String, String>) request.getAttribute(TEMP_TO_MAP_ATTRIBUTE_NAME);

        if (map != null) {
            request.removeAttribute(TEMP_TO_MAP_ATTRIBUTE_NAME);
        }

        return map;
    }

    private Map<String, Object> map() {
        Map<String, Object> map = (Map<String, Object>) request.getAttribute(MAP_ATTRIBUTE_NAME);
        if (map != null) {
            request.removeAttribute(MAP_ATTRIBUTE_NAME);
        }

        return map;
    }

    protected JSON success() {
        map("success", 1);

        return getJsonResult();
    }

    private JSON getJsonResult() {
        request.removeAttribute(NEWVALUE_ATTRIBUTE_NAME);

        Map<String, Object> map = map();
        map.putAll((Map<String, List>)request.getAttribute(ARRAY_ATTRIBUTE_NAME));

        request.removeAttribute(ARRAY_ATTRIBUTE_NAME);

        return JSONObject.fromObject(map);
    }

    protected JSON error(String msg) {
        map("success", 0);
        map("msg", msg);

        return getJsonResult();
    }
    protected JSON error(Exception exception, String msg) {
        map("success", 0);
        map("msg", msg);
        logger.error(exception.getMessage(), exception);
        return getJsonResult();
    }

}
