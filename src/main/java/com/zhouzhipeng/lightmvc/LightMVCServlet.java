package com.zhouzhipeng.lightmvc;

import com.zhouzhipeng.lightmvc.controller.Controller;
import com.zhouzhipeng.lightmvc.monitor.Contrail;
import com.zhouzhipeng.lightmvc.misc.LightConfig;
import com.zhouzhipeng.lightmvc.monitor.Monitor;
import com.zhouzhipeng.lightmvc.service.Service;
import com.zhouzhipeng.lightmvc.utils.DBUtils;
import com.zhouzhipeng.lightmvc.utils.LightUtils;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.*;

/**
 * 上下文servlet，负责初始化数据和分发请求
 * <p/>
 * User: zhouzhipeng
 * Date: 15/7/18:23:20
 */
public final class LightMVCServlet extends HttpServlet {


    private Logger log = Logger.getLogger(LightMVCServlet.class);

    private Map<String, Controller> controllers;

    private static LightConfig _lightConfig;

    public static LightConfig lightConfig() {
        return _lightConfig;
    }

    private static Monitor monitor;

    private static ThreadLocal<Contrail> contrailThreadLocal = new ThreadLocal<Contrail>();

    public static Contrail getThreadLocalContrail(){
        return contrailThreadLocal.get();
    }

    /**
     * 监视器是否打开
     *
     * @return
     */
    public static boolean isMonitorOn() {
        return monitor != null;
    }



    @Override
    public void destroy() {

        if (controllers != null)
            controllers.clear();

        DBUtils.destroy();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {

        try {

            String configClass = config.getInitParameter("configClass");

            if(configClass==null){
                throw new RuntimeException("please configure the init-param in the LightMVCServlet!");
            }
            Class<?> configClazz = Class.forName(configClass);

            if (configClazz.getSuperclass() == LightConfig.class) {
                LightConfig lightConfig = (LightConfig) configClazz.newInstance();
                //初始化数据库
                DBUtils.init(lightConfig.db_driver(), lightConfig.db_url(), lightConfig.db_user(), lightConfig.db_pwd());

                _lightConfig = lightConfig;

                monitor = lightConfig.monitor();

            } else {
                throw new RuntimeException("the class " + configClass + " must implement com.zhouzhipeng.lightmvc.misc.LightConfig");
            }


            //扫描controller和service
            Method initMethod = Service.class.getDeclaredMethod("init", List.class,ThreadLocal.class);
            initMethod.setAccessible(true);
            List<Class<? extends Service>> services = new ArrayList<Class<? extends Service>>();

            controllers = new HashMap<String, Controller>();

            String scanPath = _lightConfig.scanPath();

            String path = scanPath.replaceAll("\\.", "/");

            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            URL url = classloader.getResource(path);

            List<Class<?>> classes = getClasses(new File(url.getFile()), scanPath);
            for (Class<?> aClass : classes) {

                com.zhouzhipeng.lightmvc.URL annotation = aClass.getAnnotation(com.zhouzhipeng.lightmvc.URL.class);
                if ((contains(aClass, Controller.class) && (aClass.getModifiers() & Modifier.ABSTRACT) != Modifier.ABSTRACT)) {

                    if (annotation != null) {
                        //放入map
                        if (controllers.get(annotation.value()) != null) {
                            throw new RuntimeException("more than one url mapping [" + annotation.value() + "] is found on class " + controllers.get(annotation.value()).toString());
                        }
                        controllers.put(annotation.value(), (Controller) aClass.newInstance());
                    } else {
                        //default url path
                        String uri = parseDefaultUrlPath(aClass.getSimpleName());
                        controllers.put(uri, (Controller) aClass.newInstance());
                    }
                } else if (contains(aClass, Service.class)) {
                    services.add((Class<? extends Service>) aClass);
                }

            }

            //services
            log.info("init services");
            initMethod.invoke(null, services,contrailThreadLocal);

            classes.clear();
            System.gc();

            log.info("find controllers :" + controllers);
            log.info("find services :" + services);


            //启动回调
            log.info("启动回调...");
            _lightConfig.onStartUp();

        } catch (Exception e) {
            log.error("lightmvc框架启动失败!!");
            log.error(e.getMessage(), e);
            System.exit(0);
        }

    }

    private boolean contains(Class<?> clazz, Class<?> superClass) {

        for (Class<?> aClass : clazz.getInterfaces()) {

            if (aClass == superClass) {
                return true;
            }
        }


        if (clazz.getSuperclass() == superClass) {
            return true;
        }

        return clazz.getSuperclass() != null && contains(clazz.getSuperclass(), superClass);

    }

    private String parseDefaultUrlPath(String simpleName) {

        if (simpleName.contains("Controller")) {
            simpleName = simpleName.substring(0, simpleName.length() - "Controller".length());
        }

        //首字母小写
        if (simpleName.charAt(0) < 'a') {
            simpleName = (char) ((int) simpleName.charAt(0) + 32) + simpleName.substring(1);
        }

        if (simpleName.equals(Controller.DEFAULT_METHOD_NAME)) {
            return "/";
        }

        return "/" + simpleName;
    }

    /**
     * 迭代查找类
     *
     * @param dir
     * @param pk
     * @return
     * @throws ClassNotFoundException
     */
    private static List<Class<?>> getClasses(File dir, String pk) throws ClassNotFoundException {
        List<Class<?>> classes = new ArrayList<Class<?>>();
        if (dir == null || !dir.exists()) {
            return classes;
        }
        for (File f : dir.listFiles()) {
            if (f.isDirectory()) {
                classes.addAll(getClasses(f, pk + "." + f.getName()));
            }
            String name = f.getName();
            if (name.endsWith(".class")) {
                classes.add(Class.forName(pk + "." + name.substring(0, name.length() - 6)));
            }
        }
        return classes;
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {


            String uri = null;

            String requestURI = req.getRequestURI();
            String servletPath = req.getServletPath();

            if (requestURI.equals(servletPath)) {
                uri = servletPath;
                req.setAttribute(Controller.SUFFIX_REQ_URI, servletPath);
                req.setAttribute(Controller.FULL_REQ_URI,servletPath);
            } else {

                if(servletPath.equals("/")){
                    uri = requestURI;//
                }else{
                    uri=requestURI.substring(req.getServletPath().length());
                }

                if (uri.charAt(uri.length() - 1) == '/') {
                    uri = uri.substring(0, uri.length() - 1);
                }

                int i = uri.lastIndexOf("/");

                req.setAttribute(Controller.FULL_REQ_URI, uri);
                if (i > 0) {
                    req.setAttribute(Controller.SUFFIX_REQ_URI, uri.substring(i));
                    uri = uri.substring(0, i);
                } else {
                    req.setAttribute(Controller.SUFFIX_REQ_URI, "/");
                }

            }

            Controller instance = controllers.get(uri);
            if (instance != null) {
                if (isMonitorOn()) {
                    log.info("++++++++ begin contrail ++++++++++");

                    Contrail contrail = new Contrail();
                    contrail.setParameters(req.getParameterMap());
                    contrail.setRequestUrl(requestURI);
                    contrail.setStartTime(new Date().getTime());
                    contrail.setCpuTime(LightUtils.getCPUTime());
                    contrail.setMemorySize(LightUtils.getMemory());
                    contrail.setList(new ArrayList<Contrail.Entity>());
                    contrailThreadLocal.set(contrail);


                    log.info("contrail>>"+contrail);
                }

                //dispatcher
                instance.handle(req, resp);


                if (isMonitorOn()) {
                    Contrail contrail = contrailThreadLocal.get();

                    contrail.setEndTime(new Date().getTime());
                    contrail.setCpuTime(LightUtils.getCPUTime() - contrail.getCpuTime());
                    contrail.setMemorySize(contrail.getMemorySize()-LightUtils.getMemory() );


                    log.info("------- end contrail -------");
                    log.info("contrail>>"+contrail);


                    log.info("callback monitor");
                    if(monitor!=null){
                        monitor.monitor(contrail);
                    }

                }
            } else {

                resp.setStatus(404);
            }

        } catch (Exception e) {

            resp.setStatus(500);
            log.error(e.getMessage(), e);

            if (isMonitorOn()) {
                Contrail contrail = contrailThreadLocal.get();

                contrail.setEndTime(new Date().getTime());
                contrail.setCpuTime(LightUtils.getCPUTime() - contrail.getCpuTime());
                contrail.setMemorySize(contrail.getMemorySize()-LightUtils.getMemory() );

                contrail.setException(e);

                log.info("------- end contrail -------");
                log.info("contrail>>"+contrail);


                log.info("callback monitor");
                if(monitor!=null){
                    monitor.monitor(contrail);
                }

            }
        }finally {
            contrailThreadLocal.remove();
        }

    }

}
