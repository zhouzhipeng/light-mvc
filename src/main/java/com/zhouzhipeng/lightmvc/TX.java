package com.zhouzhipeng.lightmvc;

import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

/**
 * 事务注解:使用事务
 * User: zhouzhipeng
 * Date: 15/7/19:00:01
 */
@java.lang.annotation.Target({ElementType.METHOD})
@java.lang.annotation.Retention(RetentionPolicy.RUNTIME)
public @interface TX {
//    String value();
}
