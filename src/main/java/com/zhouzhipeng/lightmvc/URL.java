package com.zhouzhipeng.lightmvc;

import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

/**
 * TODO:写描述
 * User: zhouzhipeng
 * Date: 15/7/19:00:01
 */
@java.lang.annotation.Target({ElementType.TYPE})
@java.lang.annotation.Retention(RetentionPolicy.RUNTIME)
public @interface URL {
    String value();
}
