package com.zhouzhipeng.lightmvc.model;

import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * TODO:写描述
 * User: zhouzhipeng.com
 * Date: 15/8/10:14:03
 */
public class Cond implements Serializable {
    public static final String gt = ">";
    public static final String ge = ">=";
    public static final String eq = "=";
    public static final String lt = "<";
    public static final String le = "<=";
    public static final String ne = "!=";

    public Cond() {
    }

    private String table;

    private HashMap<String, String> modelToTable;

    public void setModelToTable(HashMap<String, String> modelToTable) {
        this.modelToTable = modelToTable;

    }

    public com.zhouzhipeng.lightmvc.model.Cond setTableClass(Class clazz) {
        table = clazz.getSimpleName();
        return this;
    }


    private List<String> setList = new ArrayList<String>();
    private List<String> whereList = new ArrayList<String>();

    public com.zhouzhipeng.lightmvc.model.Cond set(String name, Object value) {
        name = M.mappingName(name, modelToTable);

        if (value instanceof String) {
            setList.add(name + " = '" + value + "'");
        } else {
            setList.add(name + " = " + value);
        }
        return this;
    }

    public com.zhouzhipeng.lightmvc.model.Cond where(String name, String op, Object value) {
        name = M.mappingName(name, modelToTable);

        if (value instanceof String) {
            whereList.add(name + " " + op + " '" + value + "'");
        } else {
            whereList.add(name + " " + op + " " + value);
        }
        return this;
    }

    public String getSql() {
        return toString();
    }

    @Override
    public String toString() {
        if (table == null || setList.isEmpty() || whereList.isEmpty()) {
            throw new RuntimeException("table or set or where closure is required!");
        }

        String sql = "update $1 set $2 where $3";

        sql = sql.replace("$1", table).replace("$2", StringUtils.join(setList, ",")).replace("$3", StringUtils.join(whereList, " and "));

        return sql;
    }
}
