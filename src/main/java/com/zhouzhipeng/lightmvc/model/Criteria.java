package com.zhouzhipeng.lightmvc.model;

import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * TODO:写描述
 * User: zhouzhipeng.com
 * Date: 15/8/10:14:05
 */
public class Criteria implements Serializable {
    public String getSql() {
        return toString();
    }

    public static final int ASC = 0x1;
    public static final int DESC = 0x2;

    private String tableName;

    private String projects = "*"; //查询的字段
    private List<String> segments = new ArrayList<String>(); //sql片段
    private String groupBy = "";
    private String orderBy = "";
    private String limit = "";

    private String sql = null;

    private Class<?> objClass = null;

    public Class<?> objClass() {
        return this.objClass;
    }

    private HashMap<String, String> modelToTable;

    public void setModelToTable(HashMap<String, String> modelToTable) {
        this.modelToTable = modelToTable;

    }


    public Criteria() {

    }

    public void setClazz(Class<?> clazz) {

        tableName = clazz.getSimpleName();
        this.objClass = clazz;
    }


    public Criteria eq(String fieldname, Object value) {
        return common(fieldname, value, "=");
    }

    private Criteria common(String fieldname, Object value, String op) {
        fieldname = M.mappingName(fieldname, modelToTable);


        if (value instanceof String) {
            segments.add(fieldname + " " + op + " '" + value + "'");
        } else {
            segments.add(fieldname + " " + op + " " + value);
        }

        segments.add("and");

        return this;
    }

    public Criteria like(String fieldname, Object value) {
        return common(fieldname, value, "like");
    }


    public Criteria nq(String fieldname, Object value) {
        return common(fieldname, value, "!=");
    }

    public Criteria lt(String fieldname, Object value) {
        return common(fieldname, value, "<");
    }

    public Criteria gt(String fieldname, Object value) {
        return common(fieldname, value, ">");
    }

    public Criteria le(String fieldname, Object value) {
        return common(fieldname, value, "<=");
    }

    public Criteria ge(String fieldname, Object value) {

        return common(fieldname, value, ">=");
    }

    public Criteria in(String fieldname, Object... values) {

        String c = "(";
        for (Object val : values) {
            if (val instanceof String) {
                c += "'" + val + "',";
            } else {
                c += val + ",";
            }
        }

        c = c.substring(0, c.length() - 1);

        c += ")";

        return common(fieldname, c, "in");
    }

    public Criteria sql(String sqlStr) {
        if(!sqlStr.startsWith("select")) throw new IllegalArgumentException("sqlStr");
        sql = sqlStr;
        return this;
    }

    public Criteria order(String fieldname, int order) {

        orderBy = "order by " + fieldname + " " + (order == ASC ? "ASC" : "DESC");

        return this;
    }


    /**
     * 分页
     *
     * @param offset
     * @param max
     * @return
     */

    public Criteria limit(int offset, int max) {
        limit = "limit " + offset + "," + max;
        return this;
    }

    /**
     * 投影列
     *
     * @param cols
     * @return
     */
    public Criteria project(String... cols) {
        projects = StringUtils.join(cols, ",");
        return this;
    }

    public Criteria count() {
        projects = "count(*)";
        return this;
    }

    public Criteria or() {

        if (!segments.isEmpty()) {
            String s = segments.get(segments.size() - 1);
            if (s.equals("and")) {
                //remove
                segments.remove(segments.size() - 1);
            }
        }
        segments.add("or");
        return this;
    }

    /**
     * left括号
     *
     * @return
     */
    public Criteria L() {
        segments.add("(");

        return this;
    }


    /**
     * right括号
     *
     * @return
     */
    public Criteria J() {
        if (!segments.isEmpty()) {
            if (segments.get(segments.size() - 1).equals("and")) {
                //remove
                segments.remove(segments.size() - 1);
            }
        }
        segments.add(")");
        segments.add("and");
        return this;
    }

    public Criteria group(String... cols) {
        groupBy = "group by (" + StringUtils.join(cols, ",") + ")";
        return this;
    }


    @Override
    public String toString() {
        if (sql == null) {
            sql = "select $1 from $2 where $3 $4 $5 $6";
            String $1 = projects;
            String $2 = tableName;
            String $3 = "";
            if (!segments.isEmpty()) {
                if (segments.get(segments.size() - 1).equals("and")) {
                    //remove
                    segments.remove(segments.size() - 1);
                }

                $3 = StringUtils.join(segments, " ");
            }

            String $4 = groupBy;
            String $5 = orderBy;
            String $6 = limit;

            sql = sql.replace("$1", $1).replace("$2", $2).replace("$3", $3)
                    .replace("$4", $4).replace("$5", $5).replace("$6", $6);

        }

        return sql;
    }


}