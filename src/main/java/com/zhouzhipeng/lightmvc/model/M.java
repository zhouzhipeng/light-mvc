package com.zhouzhipeng.lightmvc.model;

import com.zhouzhipeng.lightmvc.utils.DBUtils;
import com.zhouzhipeng.lightmvc.utils.MUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * dao操作接口
 * <p/>
 * CRUD
 * Create ,Retrieve,Update,Delete
 * <p/>
 * User: zhouzhipeng
 * Date: 15/7/24:11:15
 */
public final class M {

    public static final String PRIMARY_KEY_NAME = "id";

    private static Logger log = Logger.getLogger(M.class);

    public static <T> long c(T instance, String primaryKeyFieldName, HashMap<String, String> modelToTable) throws Exception {

        Class<?> aClass = instance.getClass();
        String tableName = mappingName(aClass.getSimpleName(), modelToTable);

        String insertSql = "insert into " + tableName + "(";

        List<String> names = new ArrayList<String>();
        List<Object> values = new ArrayList<Object>();

        for (Field field : aClass.getDeclaredFields()) {
            if (Modifier.isPrivate(field.getModifiers())) {
                if (field.getName().toLowerCase().equals(primaryKeyFieldName)) {
                    continue;
                }
                field.setAccessible(true);

                Object val = field.get(instance);
                if(val!=null) {
                    names.add(mappingName(field.getName(), modelToTable));
                    values.add(getActual(val));
                }
            }
        }

        insertSql += StringUtils.join(names, ",");

        insertSql += ") values(";

        insertSql += MUtils.join(values, ",");

        insertSql += ")";

        log.info(insertSql);

        return DBUtils.<Long>execute(insertSql);


    }

    private static Object getActual(Object val) {

        if(val instanceof Date){
            return jetbrick.util.DateUtils.format((Date)val,"yyyy-MM-dd HH:mm:ss");
        }

        return val;
    }

    public static <T> T r(Class<T> type, String primaryKeyFieldName, HashMap<String, String> modelToTable, long id) throws Exception {
        primaryKeyFieldName=mappingName(primaryKeyFieldName,modelToTable);

        T instance = type.newInstance();
        String tableName = mappingName(type.getSimpleName(), modelToTable);

        String selectSql = "select * from " + tableName + " where " + primaryKeyFieldName + "=" + id;


        log.info(selectSql);

        Map map = DBUtils.<List<Map>>execute(selectSql).get(0);

        fillProperty(type, modelToTable, instance, map);


        return instance;
    }

    private static  void fillProperty(Class type, HashMap<String, String> modelToTable, Object instance, Map map) throws IllegalAccessException {
        for (Field field : type.getDeclaredFields()) {
            if (Modifier.isPrivate(field.getModifiers())) {
                field.setAccessible(true);
                String name=mappingName(field.getName(), modelToTable);
                if(map.containsKey(name)) {
                    field.set(instance, map.get(name));
                }
            }
        }
    }

    public static String mappingName(String key, HashMap<String, String> modelToTable) {
        if (modelToTable == null) {
            return key;
        }
        String val = modelToTable.get(key);

        if (val == null) {
            val = key;
        }
        return val;
    }

    public static <T> List<T> r(Criteria cri, HashMap<String, String> modelToTable) throws Exception {
        cri.setModelToTable(modelToTable);
        String sql = cri.toString();

        log.info(sql);

        Class<?> aClass = cri.objClass();


        List<T> list = new ArrayList<T>();
        for (Map map : DBUtils.<List<Map>>execute(cri.toString())) {
            T instance = (T) aClass.newInstance();
            fillProperty( aClass, modelToTable, instance, map);

            list.add(instance);
        }
        return list;
    }

    public static <T> List<T> r(Class<T> clazz, Criteria cri, HashMap<String, String> modelToTable) throws Exception {
        cri.setClazz(clazz);
        return r(cri, modelToTable);
    }

    public static <T> void u(T instance, String primaryKeyFieldName, HashMap<String, String> modelToTable) throws Exception {
        Class<?> aClass = instance.getClass();
        String tableName = aClass.getSimpleName();

        String updateSql = "update " + tableName + " set ";

        Object id = null;
        for (Field field : aClass.getDeclaredFields()) {
            field.setAccessible(true);
            if (!field.getName().toLowerCase().equals(primaryKeyFieldName)) {
                updateSql += mappingName(field.getName(), modelToTable) + "='" + field.get(instance) + "',";
            } else {
                id = field.get(instance);
            }
        }


        updateSql = updateSql.substring(0, updateSql.length() - 1);

        updateSql += " where " + mappingName(primaryKeyFieldName,modelToTable) + "=" + id;

        log.info(updateSql);

        DBUtils.execute(updateSql);

    }

    public static void u(Cond cond,HashMap<String, String> modelToTable) throws Exception {
        cond.setModelToTable(modelToTable);
        String updateSql = cond.toString();

        log.info(updateSql);

        DBUtils.execute(updateSql);

    }


    public static <T> void d(Class<T> type, String primaryKeyFieldName,HashMap<String, String> modelToTable, long id) throws Exception {
        String tableName = mappingName(type.getSimpleName(),modelToTable) ;

        String deleteSql = "delete from " + tableName + " where " + mappingName(primaryKeyFieldName,modelToTable) + "=" + id;

        log.info(deleteSql);

        DBUtils.execute(deleteSql);
    }

}
