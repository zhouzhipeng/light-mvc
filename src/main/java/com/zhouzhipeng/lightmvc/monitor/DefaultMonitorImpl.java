package com.zhouzhipeng.lightmvc.monitor;

import com.zhouzhipeng.lightmvc.utils.DBUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 * TODO:写描述
 * User: zhouzhipeng.com
 * Date: 15/8/6:16:00
 */
public class DefaultMonitorImpl implements Monitor {
    @Override
    public void monitor(Contrail contrail) {
        //写入db

        try {
            String parameters=JSONObject.fromObject(contrail.getParameters()).toString();
            String list=JSONArray.fromObject(contrail.getList()).toString();
            String exception=null;

            if(contrail.getException()!=null) {
                ByteArrayOutputStream buf = new java.io.ByteArrayOutputStream();
                contrail.getException().printStackTrace(new java.io.PrintWriter(buf, true));
                exception = buf.toString();
                buf.close();
            }





            DBUtils.execute("insert into Contrail(requestUrl,parameters,`list`,startTime,endTime,memorySize,cpuTime,totalTime,`exception`) values(?,?,?,?,?,?,?,?,?)",
                    contrail.getRequestUrl(), parameters, list,contrail.getStartTime(),contrail.getEndTime(),contrail.getMemorySize(),contrail.getCpuTime(),(contrail.getEndTime()-contrail.getStartTime()),exception);



        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
