package com.zhouzhipeng.lightmvc.monitor;

/**
 * 应用监控
 * User: zhouzhipeng.com
 * Date: 15/8/4:17:39
 */
public interface Monitor {

    /**
     * 监控回调
     * @param contrail ： 请求轨迹对象，包含本次请求的cpu、内存状态
     */
    void monitor(Contrail contrail);
}
