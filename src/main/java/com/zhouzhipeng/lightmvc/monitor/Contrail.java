package com.zhouzhipeng.lightmvc.monitor;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 一次web请求的调用轨迹，记录如：什么controller的什么方法在什么时间，有什么参数，结束时间等。
 * User: zhouzhipeng.com
 * Date: 15/8/4:16:51
 */
public class Contrail implements Serializable{

    public static class Entity  implements Serializable{
        private String method;
        private long startTime;
        private long endTime;
        private Object[] inputs;
        private Object output;

        public String getMethod() {
            return method;
        }

        public void setMethod(String method) {
            this.method = method;
        }

        public long getStartTime() {
            return startTime;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public long getEndTime() {
            return endTime;
        }

        public void setEndTime(long endTime) {
            this.endTime = endTime;
        }

        public Object[] getInputs() {
            return inputs;
        }

        public void setInputs(Object[] inputs) {
            this.inputs = inputs;
        }

        public Object getOutput() {
            return output;
        }

        public void setOutput(Object output) {
            this.output = output;
        }

        @Override
        public String toString() {
            return "Entity{" +
                    "method='" + method +"'"+
                    ", startTime=" + startTime +
                    ", endTime=" + endTime +
                    ", inputs=" + Arrays.toString(inputs) +
                    ", output=" + output +
                    '}';
        }
    }


    private String requestUrl;
    private Map parameters;
    private List<Entity> list;
    private long startTime;
    private long endTime;
    private long memorySize;
    private long cpuTime;
    private Exception exception;

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public Map getParameters() {
        return parameters;
    }

    public void setParameters(Map parameters) {
        this.parameters = parameters;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public List<Entity> getList() {
        return list;
    }

    public void setList(List<Entity> list) {
        this.list = list;
    }

    /**
     * 本次请求的内存使用量，单位：字节
     * @return
     */
    public long getMemorySize() {
        return memorySize;
    }

    public void setMemorySize(long memorySize) {
        this.memorySize = memorySize;
    }

    /**
     * cpu执行时间 ,单位：纳秒
     * @return
     */
    public long getCpuTime() {
        return cpuTime;
    }

    public void setCpuTime(long cpuTime) {
        this.cpuTime = cpuTime;
    }

    @Override
    public String toString() {
        return "Contrail{" +
                "requestUrl='" + requestUrl + '\'' +
                ", parameters=" + parameters +
                ", list=" + list +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", memorySize=" + memorySize +
                ", cpuTime=" + cpuTime +
                ", exception=" + exception +
                '}';
    }
}
