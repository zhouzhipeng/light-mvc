CREATE TABLE `contrail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requestUrl` varchar(255) DEFAULT NULL,
  `parameters` text,
  `list` text,
  `startTime` bigint(20) DEFAULT NULL,
  `endTime` bigint(20) DEFAULT NULL,
  `memorySize` bigint(20) DEFAULT NULL,
  `cpuTime` bigint(20) DEFAULT NULL,
  `totalTime` bigint(20) DEFAULT NULL,
  `exception` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;