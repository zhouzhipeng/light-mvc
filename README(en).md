# lightmvc
1.Lightmvc is a super lightweight java MVC framework ! No xml configurations!No annotations!Very easy to use! Only use what you really need!

2.Accord with the COC principle.
 
 
##Overview
###1.Controller
To be a Controller ,your class must extends [JSONController|JSONPController|JetxController] any one of them,just choose 
your correct response type: json? jsonp? or html template.

If you think it's not enough to fit your web application,you can implement your own base Controller just extends the 
com.zhouzhipeng.lightmvc.BaseController.

About the controller's visited path, the default , if your Controller's name is YourDemoController just like below code block

    public YourDemoController extends JetxController{
        @Override
        public void index(){
            
        }
        
        public void test(){
            
        }
    }
    
so the method "test()" action url is : /yourDemo/test.And the method "index()" is only one default method in Controller ,the uri
is /yourDemo/ or /yourDemo.And you can have a Controller called "IndexController" and there will be only one action method in it "index()",
matches the url /. 

If you want to configure your own style url name,just use the com.zhouzhipeng.lightmvc.URL annotation,like this:
    
    @URL("/anyUrlName")
    public YourDemoController extends JetxController{
            @Override
            public void index(){
                
            }
            
            @URL("/any")
            public void test(){
                
            }
    }
    
The method "test()" will match /anyUrlName/any . If you dont want to make if fuzzy,just use the default arrange.


###2.Service
As i think, there has only two type of services, one is  Dao-Service which contains some CRUD methods and one is Non-Dao-Service which
contains no CRUD operation methods.

When your service 



##Steps
###1.configure the web.xml
    
    <servlet>
        <servlet-name>LightMVCServlet</servlet-name>

        <servlet-class>com.zhouzhipeng.lightmvc.LightMVCServlet</servlet-class>
        <init-param>
            <param-name>configClass</param-name>
            <param-value>demo.DemoConfig</param-value>
        </init-param>
        <load-on-startup>1</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>LightMVCServlet</servlet-name>
        <url-pattern>/*</url-pattern>
    </servlet-mapping>
    
    
###2.implement the class demo.DemoConfig

    public class  DemoConfig extends LightConfig {
        @Override
        public String db_url() {
            return YOUR_DB_CONN_URL;//eg."jdbc:mysql://****";
        }
    
        @Override
        public String db_user() {
            return YOUR_DB_USER;
        }
    
        @Override
        public String db_pwd() {
            return YOUR_DB_PWD;
        }
    
        @Override
        public String scanPath() {
            // the lightmvc scan package path,put your controllers and services under the scan path 
            // or sub pathes
            return "com.zhouzhipeng.lightmvcdemo";
        }

    }